<?php

namespace Rhinos\UpdateLogging\app\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UpdateStepLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            // Update Step Log details
            'id'    => $this->id,
            'update_log_id'  => $this->update_log_id,
            'update_step_id'  => $this->update_step_id,
            'status'  => $this->status,
            'step_start_at'  => $this->start_date,
            'step_end_at'  => $this->end_date,

            // Update Log details
            'update_log'  => $this->updateLog->id,
            'year'  => $this->updateLog->year,
            'month'  => $this->updateLog->month,
            'week'  => $this->updateLog->week,
            'update_log_frequency'  => $this->updateLog->frequency,
            'start_date'  => $this->updateLog->start_date,
            'end_date'  => $this->updateLog->end_date,

            // Update details
            'update_id'  => $this->updateLog->parentUpdate->id,
            'title'  => $this->updateLog->parentUpdate->alias,
            'description'  => $this->updateLog->parentUpdate->description,
            'update_frequency'  => $this->updateLog->parentUpdate->frequency,
            'active'  => $this->updateLog->parentUpdate->active,

            //Update Step details
            'service'  => $this->updateStep->type,
            'method'  => $this->updateStep->alias,
            'update_step_description'  => $this->updateStep->description,
        ];
    }
}
