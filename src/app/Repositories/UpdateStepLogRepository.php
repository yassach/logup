<?php

namespace Rhinos\UpdateLogging\app\Repositories;

use Rhinos\UpdateLogging\app\Models\UpdateLog;
use Rhinos\UpdateLogging\app\Models\UpdateStep;
use Rhinos\UpdateLogging\app\Models\UpdateStepLog;

class UpdateStepLogRepository
{

    /**
     * @param UpdateLog $logUpdate
     * @param UpdateStep $updateStep
     * @return UpdateStepLog
     */
    public static function start(UpdateLog $logUpdate, UpdateStep $updateStep): UpdateStepLog
    {

        $updateStepLog = new UpdateStepLog();

        $updateStepLog->update_log_id = $logUpdate->id;
        $updateStepLog->update_step_id = $updateStep->id;

        $updateStepLog->save();

        return $updateStepLog;
    }

    /**
     * @param $id
     */
    public static function end($id, $status, $statusDetails = '')
    {
        UpdateStepLog::findOrFail($id)
            ->update([
                'end_date' => now(),
                'status' => $status,
                'status_details' => $statusDetails,
            ]);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function listAllStepLogs()
    {
        return UpdateStepLog::with(['updateLog.parentUpdate', 'updateStep'])->paginate(3);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function findStepLog($id)
    {
        return UpdateStepLog::with(['updateLog.parentUpdate', 'updateStep'])
            ->where('id', $id)
            ->get();
    }

//$updateStepLog->owner_id = $ownerId; // Auth::id()
//
//$updateStepLog->update_id = $update->id;
//$updateStepLog->year = $options['year'] ?? date('Y');
//$updateStepLog->month = $options['month'] ?? date('m');
//$updateStepLog->week = $options['week'] ?? date('W');
//$updateStepLog->frequency = $update->frequency ?? null; //
//$updateStepLog->data_date = $options['data_date'] ?? null; // par defeaut date hier else option


}
