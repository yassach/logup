<?php

namespace Rhinos\UpdateLogging\app\Repositories;

use Illuminate\Support\Facades\Validator;
use Rhinos\UpdateLogging\app\Models\UpdateStep;
use Rhinos\UpdateLogging\app\Exceptions\UpdateNotFoundException;

class UpdateStepRepository
{

    /**
     * @param String $alias
     * @param $id
     * @return mixed
     * @throws UpdateNotFoundException
     */
    public static function findByAlias(String $alias, $id)
    {
        $updateStep = UpdateStep::where('update_id', $id)
            ->where('alias', $alias)
            ->first();
        if (!$updateStep) {
            throw new UpdateNotFoundException('update step is not found by alias '. $alias);
        }

        return $updateStep;
    }

    /**
     * @param String $name
     * @param $id
     * @return mixed
     * @throws UpdateNotFoundException
     */
    public static function findByName(String $name, $id)
    {
        $updateStep = UpdateStep::where('update_id', $id)
            ->where('name', $name)
            ->first();
        if (!$updateStep) {
            throw new UpdateNotFoundException('update step is not found by name '. $name);
        }

        return $updateStep;
    }

    /**
     * @param $id
     * @return mixed
     * @throws UpdateNotFoundException
     */
    public static function getUpdateSteps($id)
    {
        $updateSteps = UpdateStep::where('update_id', $id)
            ->where('active', 1)
            ->select('alias', 'type')
            ->get();

        if (!$updateSteps) {
            throw new UpdateNotFoundException('update steps aren\'t found by the id '. $id);
        }

        return $updateSteps;
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function store($request, $className): \Illuminate\Http\JsonResponse
    {

        /**
         * Check if update exists
         */
        $update = UpdateRepository::findById($request->update_id);
        if (!$update)
        {
            return response()->json([
                'message' => 'La mise à jour '. $request->update_id .' est introuvable!',
                'data' => null
            ]);
        }

        /**
         * Validate the form
         */
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'alias' => 'required | alpha_dash',
            'type' => 'required | alpha_dash',
            'description' => 'required ',
        ]);

        if ($validation->fails())
        {
            return response()->json([
                'message' => $validation->errors(),
                'data' => null
            ]);
        }

        /**
         * Create and attach the step to the update
         */
        $updateStep = new UpdateStep();
        $updateStep->update_id = $request->update_id;
        $updateStep->name = $request->name;
        $updateStep->alias = $request->alias;
        $updateStep->type = $className;
        $updateStep->order = 0;
        $updateStep->description = $request->description;
        $updateStep->save();

        return response()->json([
            'message' => 'l\'étape est attachée avec succès !',
            'data' => $updateStep
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getSteps()
    {
        $updateSteps = UpdateStep::where('active', 1)
            ->distinct()
            ->select('id', 'alias', 'type')
            ->groupBy('alias')
            ->groupBy('type')
            ->orderBy('id')
            ->get();
        return response()->json($updateSteps);
    }
}
