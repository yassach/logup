<?php

namespace Rhinos\UpdateLogging\app\Repositories;

use Illuminate\Support\Facades\Validator;
use Rhinos\UpdateLogging\app\Models\Update;
use Rhinos\UpdateLogging\app\Exceptions\UpdateNotFoundException;

class UpdateRepository
{

    /**
     * @param $id
     * @return mixed
     */
    public static function findById($id)
    {
        return Update::where('id', $id)->first();
    }

    /**
     * @param String $alias
     * @return mixed
     * @throws UpdateNotFoundException
     */
    public static function findByAlias(String $alias)
    {
        $update = Update::where('alias', $alias)->first();
        if (!$update) {
            throw new UpdateNotFoundException('update is not found by alias '. $alias);
        }

        return $update;
    }

    /**
     * @param String $name
     * @return mixed
     * @throws UpdateNotFoundException
     */
    public static function findByName(String $name)
    {
        $update = Update::where('name', $name)->first();
        if (!$update) {
            throw new UpdateNotFoundException('update is not found by name '. $name);
        }

        return $update;
    }

    /**
     * @param $request
     * @return Update
     */
    public static function store($request)
    {
        try {
            $update = Update::where('alias', $request->alias)->first();
            if ($update)
            {
                return response()->json([
                    'message' => 'l\'alias doit être unique!',
                    'data' => null
                ]);
            }
        }catch (\Exception $e)
        {
            echo $e->getMessage();
        }

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'alias' => 'required | alpha_dash',
            'description' => 'required ',
        ]);

        if ($validation->fails())
        {
            return response()->json([
                'message' => $validation->errors(),
                'data' => null
            ]);
        }

        $update = new Update();
        $update->name = $request->name;
        $update->alias = $request->alias;
        $update->description = $request->description;
        $update->frequency = $request->frequency;
        $update->save();

        return response()->json([
            'message' => 'mise à jour créée avec succès',
            'data' => $update
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getUpdates()
    {
        $updates = Update::where('active', 1)->paginate(3);
        return response()->json($updates);
    }
}
