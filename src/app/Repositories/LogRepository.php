<?php
namespace Rhinos\UpdateLogging\app\Repositories;

use Rhinos\UpdateLogging\app\Models\Update;
use Rhinos\UpdateLogging\app\Models\UpdateLog;

class LogRepository
{

    /**
     * @param array $options
     * @param Update $update
     * @param null $ownerId
     * @return UpdateLog
     */
    public static function start(array $options, Update $update, $ownerId = null): UpdateLog
    {

        $updateLog = new UpdateLog();
        $updateLog->update_id = $update->id;
        $updateLog->year = $options['year'] ?? date('Y');
        $updateLog->month = $options['month'] ?? date('m');
        $updateLog->week = $options['week'] ?? date('W');
        $updateLog->frequency = $update->frequency ?? null; //
        $updateLog->data_date = $options['data_date'] ?? null; // par defeaut date hier else option
        $updateLog->owner_id = $ownerId; // Auth::id()

        $updateLog->save();

        return $updateLog;
    }

    /**
     * @param $id
     */
    public static function end($id)
    {
        UpdateLog::findOrFail($id)->update(['end_date' => now()]);
    }

}
