<?php

namespace Rhinos\UpdateLogging\app\Exceptions;

use Exception;

class UpdateNotFoundException extends Exception
{
    //
}
