<?php

namespace Rhinos\UpdateLogging\app\Exceptions;

use Exception;

class UpdateServiceNotFoundException extends Exception
{
    //
}
