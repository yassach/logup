<?php

namespace Rhinos\UpdateLogging\app\Models;

use Illuminate\Database\Eloquent\Model;

class UpdateLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'update_id',
        'year',
        'month',
        'week',
        'frequency',
        'data_date',
        'owner_id',
        'start_date',
        'end_date',
    ];

    protected $dates = [
        'data_date',
        'start_date',
        'end_date',
    ];


    public function parentUpdate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('Rhinos\UpdateLogging\app\Models\Update', 'update_id', 'id');
    }
}
