<?php

namespace Rhinos\UpdateLogging\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Update extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'alias',
        'description',
        'frequency',
        'duration',
        'ins_type_id',
        'maintenance',
        'has_admin',
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];
}
