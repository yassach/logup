<?php

namespace Rhinos\UpdateLogging\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UpdateStepLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'update_log_id',
        'update_step_id',
        'owner_id',
        'start_date',
        'end_date',
        'status',
        'status_details',
    ];

    protected $dates = [
        'start_date',
        'end_date',
    ];
    /**
     * @return BelongsTo
     */
    public function updateLog(): BelongsTo
    {
        return $this->belongsTo('Rhinos\UpdateLogging\app\Models\UpdateLog', 'update_log_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function updateStep(): BelongsTo
    {
        return $this->belongsTo('Rhinos\UpdateLogging\app\Models\UpdateStep', 'update_step_id', 'id');
    }
}
