<?php

namespace Rhinos\UpdateLogging\app\Models;

use Illuminate\Database\Eloquent\Model;

class UpdateStep extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'update_id',
        'name',
        'alias',
        'description',
        'type',
        'order',
        'frequency',
        'duration',
        'ins_type_id',
        'active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentUpdate()
    {
//        return $this->belongsTo('App\Models\Update\Update', 'id', 'update_id');
    }
}
