<?php

namespace Rhinos\UpdateLogging\app\Services;

use App\Models\Users\User;
use App\Models\Status\Status;
use Illuminate\Support\Facades\DB;
use App\Models\Operations\Operation;
use App\Models\Users\InscritOperation;

class CustomerService
{

    public function test()
    {
        var_dump('test is running ...');
        return true;
    }

    public function check()
    {
        var_dump('check is running from the package.Services.Main ...');
        return false;
    }


    /*
     * Insertion/mise à jour des données dans customer
        MAJ de customer depuis dataflow_customers
     */
    public function updateCustomer()
    {
        # MAJ de customer depuis dataflow_customers
        DB::unprepared('
            INSERT INTO customer (id, sf_id, neo_id, business_name, siret, address, postal_code, city, country, creation_date, owner_company_code, owner_email, owner_name, owner_code, customer_profession_id, revenue_current_year, revenue_previous_year, revenue_trailing, revenue_previous_trailing, family_trailing, created_at, updated_at)

            SELECT
                IFNULL(c.id, NULL),
                dfc.sf_id,
                dfc.neo_id,
                dfc.business_name,
                dfc.siret,
                dfc.address,
                dfc.postal_code,
                dfc.city,
                dfc.country,
                dfc.creation_date,
                dfc.owner_company_code,
                dfc.owner_email,
                dfc.owner_name,
                dfc.owner_code,
                cp.id,
                dfc.ca_n,
                dfc.total_previous_year_revenue,
                dfc.twelve_months_trailing_revenue,
                dfc.previous_twelve_months_trailing_revenue,
                dfc.family_12mg,
                IFNULL(c.created_at, NOW()),
                IF(c.id IS NULL, NULL, NOW())
            FROM dataflow_customers AS dfc
                LEFT JOIN customer_profession AS cp ON cp.code = dfc.profession
                LEFT JOIN customer AS c ON dfc.sf_id = c.sf_id
            GROUP BY dfc.sf_id

            ON DUPLICATE KEY UPDATE
                business_name = VALUES(business_name),
                siret = VALUES(siret),
                address = VALUES(address),
                postal_code = VALUES(postal_code),
                city = VALUES(city),
                country = VALUES(country),
                owner_company_code = VALUES(owner_company_code),
                owner_email = VALUES(owner_email),
                owner_name = VALUES(owner_name),
                owner_code = VALUES(owner_code),
                customer_profession_id = VALUES(customer_profession_id),
                revenue_current_year = VALUES(revenue_current_year),
                revenue_previous_year = VALUES(revenue_previous_year),
                revenue_trailing = VALUES(revenue_trailing),
                revenue_previous_trailing = VALUES(revenue_previous_trailing),
                family_trailing = VALUES(family_trailing),
                updated_at = VALUES(updated_at)
        ');

        # check des codes professions : si un code n"existe pas dans customer_profession ==> alerte email
//        $customers = DB::table('customer')
//                       ->join('dataflow_customers', 'customer.sf_id', '=', 'dataflow_customers.sf_id')
//                       ->whereNull('customer_profession_id')
//                       ->where('dataflow_customers.profession', '!=', '')
//                       ->select('customer.sf_id', 'dataflow_customers.profession')
//                       ->get();
//        if ( ! empty($customers)) {
//            mail('j.truyol@rhinos.fr', 'Alerte Profession(s) inexistante(s)', $customers->toJson());
//        }

        return redirect()->route('updates.home')->with('status', 'success')->with('message', 'Mise à jour effectuée avec succès');
    }

    /*
     * Insertion/mise à jour des données dans customer_salesforce
        données prises dans dataflow_customers
     */
    public function updateCustomerSalesforce()
    {
        # MAJ de customer_salesforce depuis dataflow_customers
        /*DB::statement('
            INSERT INTO customer_salesforce (
                id,
                customer_id,
                LoyaltyAchievedGoalOnSalesOperation__c,
                LoyaltyBoosterMonth__c,
                LoyaltyCurrentSalesOperation__c,
                LoyaltyCustomerStatus__c,
                LoyaltyPointNumberExpiringThisYear__c,
                LoyaltyPointsBalance__c,
                LoyaltyURLToPersonnalSpace__c,
                created_at,
                updated_at
            )
            SELECT
                IFNULL(cs.id, NULL),
                c.id,
                dfc.LoyaltyAchievedGoalOnSalesOperation__c,
                dfc.LoyaltyBoosterMonth__c,
                dfc.LoyaltyCurrentSalesOperation__c,
                dfc.LoyaltyCustomerStatus__c,
                dfc.LoyaltyPointNumberExpiringThisYear__c,
                dfc.LoyaltyPointsBalance__c,
                dfc.LoyaltyURLToPersonnalSpace__c,
                IFNULL(cs.created_at, NOW()),
                IF(cs.id IS NULL, NULL, NOW())
            FROM dataflow_customers AS dfc
                INNER JOIN customer AS c ON dfc.sf_id = c.sf_id
                LEFT JOIN customer_salesforce cs ON c.id = cs.customer_id
            GROUP BY c.id
            ON DUPLICATE KEY UPDATE
                LoyaltyAchievedGoalOnSalesOperation__c = VALUES(LoyaltyAchievedGoalOnSalesOperation__c),
                LoyaltyBoosterMonth__c = VALUES(LoyaltyBoosterMonth__c),
                LoyaltyCurrentSalesOperation__c = VALUES(LoyaltyCurrentSalesOperation__c),
                LoyaltyCustomerStatus__c = VALUES(LoyaltyCustomerStatus__c),
                LoyaltyPointNumberExpiringThisYear__c = VALUES(LoyaltyPointNumberExpiringThisYear__c),
                LoyaltyPointsBalance__c = VALUES(LoyaltyPointsBalance__c),
                LoyaltyURLToPersonnalSpace__c = VALUES(LoyaltyURLToPersonnalSpace__c),
                updated_at = NOW()
        ');*/

        return redirect()->route('updates.home')->with('status', 'success')->with('message', 'Mise à jour effectuée avec succès');
    }

    /*
     * Insertion/mise à jour des données dans customer_contact
        données prises dans dataflow_contacts
     */
    public function updateCustomerContact()
    {
        # MAJ de customer_contact depuis dataflow_contacts
        DB::unprepared("
            INSERT INTO customer_contact (customer_id, contact_id, email, first_name, last_name, cell_phone, phone, main, created_at, updated_at)

            SELECT
                c.id,
                dfc.contact_id,
                dfc.email,
                dfc.first_name,
                dfc.last_name,
                dfc.cell_phone,
                dfc.phone,
                IF(main = 'true', 1, 0),
                NOW(),
                NOW()
            FROM dataflow_contacts AS dfc
                INNER JOIN customer AS c ON (dfc.sf_id = c.sf_id)

            ON DUPLICATE KEY UPDATE
                email = VALUES(email),
                first_name = VALUES(first_name),
                last_name = VALUES(last_name),
                cell_phone = VALUES(cell_phone),
                phone = VALUES(phone),
                main = VALUES(main),
                updated_at = NOW()
        ");

        # MAJ de customer_contact en supprimant les lignes absentes du fichier, pour les customers non inscrits (+ traitement inverse)
        # trouver une meilleure solution pour mettre à jour les contacts des comptes inscrits
        #  (si inscrit avec demande inscription et main non présent dans le fichier salesforce)
        DB::unprepared('
            UPDATE customer_contact
            SET deleted_at = IF(DATE(updated_at) = DATE(NOW()), NULL, NOW())
            WHERE (
                deleted_at IS NULL
                OR (deleted_at IS NOT NULL AND DATE(updated_at) = DATE(NOW()))
            )
            AND NOT EXISTS (SELECT 1 FROM inscrit_details WHERE inscrit_details.customer_id = customer_contact.customer_id)
        ');

        # MAJ de customer_contact_id en récupérant le main contact
        DB::unprepared('
            UPDATE customer AS c
            INNER JOIN (
                SELECT id, customer_id
                FROM customer_contact
                WHERE IFNULL(main, 0) = 1
            ) AS t2 ON c.id = t2.customer_id
            SET c.customer_contact_id = t2.id
        ');

        return redirect()->route('updates.home')->with('status', 'success')->with('message', 'Mise à jour effectuée avec succès');
    }

    /*
     * Insertion/mise à jour des données dans customer_details depuis dataflow_customer_details
     * MAJ de customer depuis customer_details : pour exclure les grands comptes
     */
    public function updateCustomerDetails()
    {
        # MAJ de customer_details depuis dataflow_customers_details
        DB::unprepared("
            INSERT INTO customer_details (id, customer_id, mrod_code, key_account, created_at, updated_at)
            SELECT
                IFNULL(cd.id, NULL),
                c.id,
                LPAD(dfc.mrod, 7, 0),
                IF(dfc.grand_compte = 'O', 1, 0),
                NOW(),
                NOW()

            FROM dataflow_customers_details AS dfc
                INNER JOIN customer AS c ON dfc.neo_id_code = c.neo_id
                LEFT JOIN customer_details AS cd ON cd.customer_id = c.id AND cd.mrod_code = LPAD(dfc.mrod, 7, 0)

            ON DUPLICATE KEY UPDATE
                key_account = VALUES(key_account),
                updated_at = VALUES(updated_at)
        ");

        # MAJ de customer_details en supprimant les lignes absentes du fichier, pour les customers non inscrits (+ traitement inverse)
        DB::unprepared('
            UPDATE customer_details
            SET deleted_at = IF(DATE(updated_at) = DATE(NOW()), NULL, NOW())
            WHERE (
                deleted_at IS NULL
                OR (deleted_at IS NOT NULL AND DATE(updated_at) = DATE(NOW()))
            )
            AND NOT EXISTS (SELECT 1 FROM inscrit_details WHERE inscrit_details.customer_id = customer_details.customer_id)
        ');

        # MAJ de customer depuis customer_details : pour pouvoir exclure les grands comptes
        DB::unprepared("
            UPDATE customer AS c
            INNER JOIN (
                SELECT customer_id, key_account
                FROM customer_details
            ) AS cd ON cd.customer_id = c.id
            SET c.key_account = cd.key_account
        ");

        return redirect()->route('updates.home')->with('status', 'success')->with('message', 'Mise à jour effectuée avec succès');
    }

    /*
     * Insertion/mise à jour des données dans customer_entity depuis dataflow_customer_entity
     */
    public function updateCustomerEntity()
    {
        # MAJ de customer_details depuis dataflow_customers_details
        DB::unprepared('
            INSERT INTO customer_entity (id, customer_id, entity_id, created_at)
            SELECT
                IFNULL(ce.id, NULL),
                c.id,
                IFNULL(e3.id, IFNULL(e2.id, e1.id)),
                "'.now()->toDateTimeString().'"

            FROM dataflow_customers AS dfc
                INNER JOIN customer AS c ON dfc.neo_id = c.neo_id
                INNER JOIN entity AS e1 ON e1.alias = dfc.owner_company_code
                INNER JOIN entity AS e2 ON CAST(e2.alias AS UNSIGNED) = CAST(dfc.owner_code AS UNSIGNED) AND e2.parent_id = e1.id
                LEFT JOIN entity AS e3 ON UPPER(e3.title) = UPPER(dfc.owner_name)
                LEFT JOIN customer_entity AS ce ON ce.customer_id = c.id AND ce.entity_id = IFNULL(e3.id, IFNULL(e2.id, e1.id))

            WHERE IF(e3.id IS NOT NULL, IF(e3.parent_id = e2.id, true, IF(e2.id IS NOT NULL, true, false)), true )
                AND dfc.owner_code <> ""
                AND dfc.owner_company_code <> ""

            GROUP BY c.id, IFNULL(e3.id, IFNULL(e2.id, e1.id))

            ON DUPLICATE KEY UPDATE
                updated_at = "'.now()->toDateTimeString().'"
        ');

        DB::unprepared('
            INSERT INTO customer_entity (id, customer_id, entity_id, created_at)
            SELECT
                IFNULL(ce.id, NULL),
                c.id,
                IFNULL(e3.id, IFNULL(e2.id, e1.id)),
                "'.now()->toDateTimeString().'"

            FROM dataflow_customers AS dfc
                INNER JOIN customer AS c ON dfc.neo_id = c.neo_id
                INNER JOIN entity AS e1 ON e1.alias = dfc.owner_company_code
                LEFT JOIN entity AS e2 ON CAST(e2.alias AS UNSIGNED) = CAST(dfc.owner_code AS UNSIGNED) AND e2.parent_id = e1.id
                LEFT JOIN entity AS e3 ON UPPER(e3.title) = UPPER(dfc.owner_name)
                LEFT JOIN customer_entity AS ce ON ce.customer_id = c.id AND ce.entity_id = IFNULL(e3.id, IFNULL(e2.id, e1.id))

            WHERE IF(e3.id IS NOT NULL, IF(e3.parent_id = e2.id, true, IF(e2.id IS NOT NULL, true, false)), true )
                AND dfc.owner_code LIKE ""
                AND dfc.owner_company_code <> ""
                AND NOT EXISTS (SELECT 1 FROM customer_entity AS ce2 INNER JOIN entity AS agence ON agence.id = ce2.entity_id WHERE ce2.customer_id = c.id AND agence.parent_id = e1.id)

            GROUP BY c.id, IFNULL(e3.id, IFNULL(e2.id, e1.id))

            ON DUPLICATE KEY UPDATE
                updated_at = "'.now()->toDateTimeString().'"
        ');

        # MAJ de customer_entity en supprimant les lignes absentes du fichier, pour les customers non inscrits (+ traitement inverse)
        DB::unprepared('
            UPDATE customer_entity
            SET deleted_at = IF(DATE(updated_at) = CURDATE() OR DATE(created_at) = CURDATE(), NULL, NOW())
            WHERE (
                deleted_at IS NULL
                OR (deleted_at IS NOT NULL AND (DATE(updated_at) = CURDATE() OR DATE(created_at) = CURDATE()))
            )
            /* AND NOT EXISTS (SELECT 1 FROM inscrit_details WHERE inscrit_details.customer_id = customer_entity.customer_id) */
        ');

        # MAJ de inscrit_entity en ajoutant les nouveaux rattachements, pour les customers inscrits
        DB::unprepared('
            INSERT IGNORE INTO inscrit_entity
                SELECT ie.id, c.ins_id, 3, ce.entity_id
                FROM customer c
                INNER JOIN customer_entity ce ON ce.customer_id = c.id AND ce.deleted_at IS NULL
                LEFT JOIN inscrit_entity ie ON ie.ins_id = c.ins_id AND ie.entity_id = ce.entity_id
                WHERE c.ins_id IS NOT NULL
        ');

        # MAJ de inscrit_entity en supprimant les rattachements supprimés, pour les customers inscrits
        DB::unprepared('
            DELETE FROM inscrit_entity WHERE EXISTS(
                SELECT 1
                FROM customer c
                INNER JOIN customer_entity ce ON ce.customer_id = c.id AND ce.deleted_at IS NOT NULL
                WHERE c.ins_id IS NOT NULL AND inscrit_entity.ins_id = c.ins_id AND inscrit_entity.entity_id = ce.entity_id)
        ');

        return redirect()->route('updates.home')->with('status', 'success')->with('message', 'Mise à jour effectuée avec succès');
    }

    /*
     * Si un customer a un ins_id, on met à jour ses infos dans inscrit
     */
    public function updateInscrit()
    {
        DB::unprepared("
            UPDATE inscrit AS i
            INNER JOIN (
                SELECT
                    id.ins_id,
                    c.business_name AS ins_raison_sociale,
                    c.address AS ins_adresse,
                    c.postal_code AS ins_cp,
                    c.city AS ins_ville
                FROM inscrit_details AS id
                    INNER JOIN customer AS c ON id.customer_id = c.id
                WHERE c.ins_id IS NOT NULL
            ) AS t2 ON (i.ins_id = t2.ins_id)

            SET
                i.ins_raison_sociale = t2.ins_raison_sociale,
                i.ins_adresse = t2.ins_adresse,
                i.ins_cp = t2.ins_cp,
                i.ins_ville = t2.ins_ville

            WHERE i.ins_demo = 0 AND i.ins_sup = 0
        ");
    }

    /*
     * Mise à jour du statut
     *      Calcul dans customer_history
     *      Mise à jour dans customer du statut "annuel"
     *      Mise à jour dans inscrit_operation du statut "annuel" pour l'opération fidélité concernée
     */
    public function updateStatus()
    {
        ## DB::statement('SET @annee = ' . (int)$this->values['year']);
        ## DB::statement('SET @mois = ' . (int)$this->values['month']);
        DB::statement('SET @annee = YEAR(NOW())');
        DB::statement('SET @mois = MONTH(NOW())');

        # MAJ en partant des données dans customer
        DB::statement("
        UPDATE customer AS t1
            INNER JOIN (
                SELECT
                    c.id,
                    status_rules.status_id AS new_status
                FROM customer AS c
                    JOIN (
                        SELECT
                            status_id, revenue, family
                        FROM operation AS o
                            INNER JOIN operation_status AS os ON o.id = os.operation_id
                            INNER JOIN operation_status_stage AS oss on os.id = oss.operation_status_id
                        WHERE o.type = 'fidelite' AND o.active
                            AND DATE(CONCAT(@annee, '-', @mois, '-01')) BETWEEN DATE(o.date_start) AND DATE(o.date_end)
                        ORDER BY os.level DESC
                    ) AS status_rules ON c.revenue_trailing >= status_rules.revenue AND c.family_trailing >= status_rules.family
                WHERE (CASE
                        WHEN @mois = 12 THEN 1
                        ELSE status_rules.status_id > IFNULL(c.status_id, 0)
                    END)
                GROUP BY c.id
            ) AS t2 ON t1.id = t2.id
        SET t1.status_id = t2.new_status,
            t1.updated_at = NOW()
        WHERE (t1.status_id IS NULL OR (CASE WHEN @mois = 12 THEN 1 WHEN t2.new_status > t1.status_id THEN 1 ELSE 0 END))
          AND NOT EXISTS(SELECT 1 FROM inscrit WHERE ins_id = t1.ins_id AND ins_demo)
        ");

        # On historise les changements de statuts
        DB::statement("
        INSERT INTO inscrit_history (ins_id, ins_id_changed, type_changed, action, created_at)

        SELECT i.ins_id, (SELECT MIN(ins_id) FROM inscrit WHERE ins_login = 'prolians@rhinos.fr'), 1, CONCAT('Changement de statut ', IFNULL(status_old.name, 'Prolians & moi'), ' > ', IFNULL(status_new.name, 'Prolians & moi')), NOW()
        FROM inscrit AS i
            INNER JOIN customer AS c on i.ins_id = c.ins_id
            INNER JOIN operation AS o on o.type = 'fidelite' AND o.active AND DATE(CONCAT(@annee, '-', @mois, '-01')) BETWEEN DATE(o.date_start) AND DATE(o.date_end)
            LEFT JOIN inscrit_operation AS io ON io.ins_id = i.ins_id AND io.operation_id = o.id
            LEFT JOIN status AS status_old on io.status_id = status_old.id
            LEFT JOIN operation_status AS os_old ON os_old.status_id = status_old.id AND os_old.operation_id = o.id
            LEFT JOIN status AS status_new ON c.status_id = status_new.id
            LEFT JOIN operation_status AS os_new ON os_new.status_id = status_new.id AND os_new.operation_id = o.id
        WHERE NOT i.ins_demo
              AND IFNULL(os_new.level, 0) > IFNULL(os_old.level, 0)
        GROUP BY i.ins_id
        ");

        # MAJ de status_id dans inscrit_operation
        DB::statement("
        UPDATE inscrit_operation AS io
            INNER JOIN (
                SELECT
                    c.ins_id,
                    c.status_id AS new_status,
                    os2.level AS new_status_level
                FROM customer AS c
                    INNER JOIN operation_status os2 on c.status_id = os2.status_id
                WHERE c.status_id IS NOT NULL
                  AND c.ins_id IS NOT NULL
            ) AS t2 ON io.ins_id = t2.ins_id
            LEFT JOIN operation_status AS os ON os.status_id = io.status_id
        SET io.status_id = IF(t2.new_status_level > IFNULL(os.level, 0), t2.new_status, io.status_id),
            io.updated_at = NOW()
        WHERE EXISTS(
            SELECT 1
            FROM operation AS o
            WHERE o.type = 'fidelite' AND o.active
                AND DATE(CONCAT(@annee, '-', @mois, '-01')) BETWEEN DATE(o.date_start) AND DATE(o.date_end)
                AND o.id = io.operation_id
        )
        ");

        # update des statuts à NULL en Prolians & moi
        DB::statement("UPDATE customer SET status_id = 1 WHERE status_id IS NULL");
    }

    /*
     * Si un customer a un ins_id, on met à jour les opérations auxquelles il est inscrit selon son statut (dans inscrit_operation)
        exemple: un plomb qui passe bronze gagne l’accès au programme de fid, opsial, mois booster…
        traiter au cas par cas selon les éligibilités des statuts aux opérations
     */
    public function updateInscritOperationFondation()
    {
        $operations = [];

        foreach (
            User::whereHas('customer', function ($query) {
                $query->whereIn('customer.status_id', [2, 3, 4]);
            })
                ->where(function ($query) {
                    $query->whereDoesntHave('operations', function ($query) {
                        $query->where('operation.type', 'fidelite')
                            ->where('year', now()->year);
                    })->orWhereDoesntHave('operations', function ($query) {
                        $query->where('operation.type', 'croissance')
                            ->where('year', now()->year);
                    })->orWhereDoesntHave('operations', function ($query) {
                        $query->where('operation.type', 'opsial')
                            ->where('year', now()->year);
                    })->orWhereDoesntHave('operations', function ($query) {
                        $query->where('operation.type', 'booster')
                            ->where('year', now()->year);
                    });
                })
                ->without('detail', 'entities', 'status')->with(['operations', 'customer'])->cursor() as $user
        ) {
            if ( ! isset($operations[$user->customer->status_id])) {
                $operations[$user->customer->status_id] = Status::find($user->customer->status_id)->current_operations;
            }

            foreach ($operations[$user->customer->status_id] as $operation) {
                if ( ! $user->operations->contains('id', $operation->id)) {
                    InscritOperation::create([
                        'ins_id'       => $user->ins_id,
                        'operation_id' => $operation->id,
                        'entity_id'    => null,
                        'status_id'    => $user->customer->status_id,
                        'active'       => in_array($operation->type, ['fidelite', 'croissance', 'opsial']) ? 1 : 0
                    ]);
                }
            }
        }
    }

    public function updateInscritOperationConstruction()
    {
        $operation_id = Operation::where('operation.type', 'ddv_ca')->where('year', now()->year)->value('id');

        foreach (
            User::whereHas('customer', function ($query) {
                $query->whereIn('customer.status_id', [1, 2]);
            })->whereDoesntHave('operations', function ($query) use ($operation_id) {
                $query->where('operation.id', $operation_id);
            })->with('customer')->cursor() as $user
        ) {
            $revenuePreviousYear = now()->month > 1 ? $user->customer->revenue_previous_year : $user->customer->revenue_current_year;
            if ($revenuePreviousYear > 5500) {
                continue;
            }

            InscritOperation::create([
                'ins_id'       => $user->ins_id,
                'operation_id' => $operation_id,
                'entity_id'    => null,
                'status_id'    => $user->customer->status_id,
                'active'       => 0
            ]);
        }
    }

    public function updateInscritOperationFinition()
    {
        $operation_id = Operation::where('operation.type', 'ddv_famille')->where('year', now()->year)->value('id');

        foreach (
            User::whereHas('customer', function ($query) {
                $query->whereIn('customer.status_id', [1, 2, 3, 4]);
            })->whereDoesntHave('operations', function ($query) use ($operation_id) {
                $query->where('operation.id', $operation_id);
            })->with('customer')->cursor() as $user
        ) {
            InscritOperation::create([
                'ins_id'       => $user->ins_id,
                'operation_id' => $operation_id,
                'entity_id'    => null,
                'status_id'    => $user->customer->status_id,
                'active'       => 0
            ]);
        }
    }
}
