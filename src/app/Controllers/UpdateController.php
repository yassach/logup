<?php

namespace Rhinos\UpdateLogging\app\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Rhinos\UpdateLogging\app\Main\UpdateService;
use Rhinos\UpdateLogging\app\Repositories\UpdateRepository;
use Rhinos\UpdateLogging\app\Resources\UpdateStepLogResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Rhinos\UpdateLogging\app\Repositories\UpdateStepRepository;
use Rhinos\UpdateLogging\app\Repositories\UpdateStepLogRepository;


/**
 * @OA\Info(title="Update API", version="1.0.0")
 */

class UpdateController extends Controller
{

    /**
     * @OA\Get(
     *     path="/update/resource.json",
     *     @OA\Response(response="200", description="list all the updates")
     * )
     */

    /**
     * Get the logs
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        try {
            $stepLogs = UpdateStepLogRepository::listAllStepLogs();
        }catch (\Exception $e)
        {
            echo $e->getMessage();
        }
        return UpdateStepLogResource::collection($stepLogs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return AnonymousResourceCollection
     */
    public function show($id): AnonymousResourceCollection
    {
        try {
            $stepLog = UpdateStepLogRepository::findStepLog($id);
        }catch (\Exception $e)
        {
            echo $e->getMessage();
        }
        return UpdateStepLogResource::collection($stepLog);
    }

    /**
     * @param Request $request
     * Create a new update (alias, name, description ...)
     */
    public function store(Request $request)
    {

        try {
            $retourne = UpdateRepository::store($request);
            return response()->json([
                'message' => $retourne
            ], 200);
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
        }

    }

    /**
     * @param Request $request
     * @param $update_id
     */
    public function attachStepToUpdate(Request $request): \Illuminate\Http\JsonResponse
    {

        /**
         * Check if directory doesn't exist yet
         * Maybe later, it would be nice to define __SERVICES__ in config file
         * And create all the necessary folder when we publish the package
         * file:///C:/Users/YAchour/Documents/dev/stim_prolians/packages/Rhinos/UpdateLogging/src/app
         */

        if (!file_exists(app_path() . '/Services/Updates'))
        {
            mkdir(app_path() . '/Services/Updates');
        }

        try {

            $className = $request->type;
            if (!str_ends_with($request->type, 'Service'))
            {
                $className .= 'Service';
            }

            // Create ClassService if not found
            UpdateService::createService(ucfirst($className));

            // Create method in the new service class
            UpdateService::createMethod(ucfirst($className), $request->alias);

            // Create Step && attach it to Update
            $retourne = UpdateStepRepository::store($request, ucfirst($className));

            return response()->json([
                'message' => $retourne
            ], 200);
        }
        catch (\Exception $e)
        {
            return response()->json($e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpdates()
    {
        try {
            return UpdateRepository::getUpdates();
        }
        catch (\Exception $e)
        {
            return response()->json($e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function getSteps()
    {
        try {
            return UpdateStepRepository::getSteps();
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
        }
    }

}
