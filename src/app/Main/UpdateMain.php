<?php

namespace Rhinos\UpdateLogging\app\Main;

use Illuminate\Support\Facades\Log;
use Mockery\Exception\BadMethodCallException;
use Rhinos\UpdateLogging\app\Repositories\LogRepository;
use Rhinos\UpdateLogging\app\Repositories\UpdateRepository;
use Rhinos\UpdateLogging\app\Repositories\UpdateStepRepository;
use Rhinos\UpdateLogging\app\Exceptions\UpdateNotFoundException;
use Rhinos\UpdateLogging\app\Repositories\UpdateStepLogRepository;
use Rhinos\UpdateLogging\app\Exceptions\UpdateServiceNotFoundException;

class UpdateMain
{
    /**
     * @param $update
     * @param array|null $options
     * @throws UpdateNotFoundException|UpdateServiceNotFoundException
     */
    public static function run($update, array $options = null)
    {

        /**
         * Store the beginning log of the update
         */
        $logUpdate = LogRepository::start($options, $update);

        /**
         * Get the service methods (alias) from DB
         */
        $updateSteps = UpdateStepRepository::getUpdateSteps($update->id);

        $methods = $updateSteps->map(function ($value) {
            return $value->alias;
        });

        $classNames = $updateSteps->map(function ($value) {
            return $value->type;
        });

        if ( !empty($options['method']) )
        {
            if ( $methods->search($options['method']) === false)
            {
                echo "\033[01;31m Method {$options['method']} not found! \033[0m\n";
                exit();
            }
            else{
                $classNames = array($classNames->get($methods->search($options['method'])));
                $methods = array($options['method']);
            }
        }

        /**
         * Iterate over the methods to execute
         */
        foreach ($methods as $key => $method)
        {

            /**
             * Instantiate Update Service dynamically
             */
            $nameSpace = 'App\\Services\\Updates\\' . $classNames[$key];

            /**
             * Check if the service exists
             */
            if (!class_exists($nameSpace)){
                throw new UpdateServiceNotFoundException('La classe '.$nameSpace.' n\'existe pas!');
            }
            $updateService = new $nameSpace;

            /**
             * Check the method exists in the service class
             */
            if (!method_exists($updateService, $method)) {
                throw new BadMethodCallException("La fonction " . $method . " n'existe pas dans la classe ".get_class($updateService));
            }

            /**
             * Get the update step of the current method
             */
            $updateStep = UpdateStepRepository::findByAlias($method, $update->id);

            /**
             * Log the update step
             */
            $updateStepLog = UpdateStepLogRepository::start($logUpdate,$updateStep);

            try {
                /**
                 * Execute the step (method/script)
                 */
                $updateService->{$method}();

                /**
                 * Log the end step with success
                 */
                UpdateStepLogRepository::end($updateStepLog->id, 'success');
            } catch (\Exception $exception)
            {
                /**
                 * Log the end step with fail
                 */
                UpdateStepLogRepository::end($updateStepLog->id, 'fail', $exception->getMessage());
            }
        }

        /**
         * Store the end log of the update
         */
        LogRepository::end($logUpdate->id);

    }


    /**
     * Create new Update
     * Think don't need this, check later!
     */
    public function createUpdate($request)
    {
        // Update by definition, is one or more services to be executed
        // So when you create an update, you give it a name, alias, description ...
        $update = UpdateRepository::store($request);

        // Then you associate it with a new service(s) or existing service(s) which in this case you need to specify the methods
        // Otherwise, if it's a new service(s), you need to add new method(s) to the new service(s)

        // First step is just to create the update (name, alias, description ... nothing fancy)
        // Then associate it with new/existing service(s) & method(s)

        // If we create a new Service class, we need to make sure it doesn't exist
        // Check the Service don't exist in DB
        // update_steps.type
        // $service = UpdateStepRepository::checkByType($type);
        // Check the Service don't exist in namespace

        // Create the service

        // Add method(s) to the service (optional)

        // Create Re
    }
}
