<?php

namespace Rhinos\UpdateLogging\app\Main;

use http\Env\Response;
use Illuminate\Support\Facades\File;
use Mockery\Exception\BadMethodCallException;
use Rhinos\RsAdmin\Models\Interne\Service;
use Rhinos\UpdateLogging\app\Repositories\LogRepository;
use Rhinos\UpdateLogging\app\Repositories\UpdateStepRepository;
use Rhinos\UpdateLogging\app\Exceptions\UpdateNotFoundException;
use Rhinos\UpdateLogging\app\Repositories\UpdateStepLogRepository;
use Rhinos\UpdateLogging\app\Exceptions\UpdateServiceNotFoundException;

class UpdateService
{
    /**
     * @param $update
     * @param array|null $options
     * @throws UpdateNotFoundException|UpdateServiceNotFoundException
     */
    public static function run($update, array $options = null)
    {

        /**
         * Store the beginning log of the update
         */
        $logUpdate = LogRepository::start($options, $update);

        /**
         * Get the service methods (alias) from DB
         */
        $updateSteps = UpdateStepRepository::getUpdateSteps($update->id);

        $methods = $updateSteps->map(function ($value) {
            return $value->alias;
        });

        $classNames = $updateSteps->map(function ($value) {
            return $value->type;
        });

        if ($options['method'])
        {
            $classNames = array($classNames->get($methods->search($options['method'])));
            $methods = array($options['method']);
        }

        /**
         * Iterate over the methods to execute
         */
        foreach ($methods as $key => $method)
        {

            /**
             * Instantiate Update Service dynamically
             */
            $nameSpace = 'Rhinos\\UpdateLogging\\app\Services\\' . $classNames[$key];

            /**
             * Check if the service exists
             */
            if (!class_exists($nameSpace)){
                throw new UpdateServiceNotFoundException('La classe '.$nameSpace.' n\'existe pas!');
            }
            $updateService = new $nameSpace;

            /**
             * Check the method exists in the service class
             */
            if (!method_exists($updateService, $method)) {
                throw new BadMethodCallException("La fonction " . $method . " n'existe pas dans la classe ".get_class($updateService));
            }

            /**
             * Get the update step of the current method
             */
            $updateStep = UpdateStepRepository::findByAlias($method, $update->id);

            /**
             * Log the update step
             */
            $updateStepLog = UpdateStepLogRepository::start($logUpdate,$updateStep);

            try {
                /**
                 * Execute the step (method/script)
                 */
                $updateService->{$method}();

                /**
                 * Log the end step with success
                 */
                UpdateStepLogRepository::end($updateStepLog->id, 'success');
            } catch (\Exception $exception)
            {
                /**
                 * Log the end step with fail
                 */
                UpdateStepLogRepository::end($updateStepLog->id, 'fail', $exception->getMessage());
            }
        }

        /**
         * Store the end log of the update
         */
        LogRepository::end($logUpdate->id);

    }

    /**
     * @param String $className
     * Create Service class if it doesn't exist
     */
    public static function createService(String $className)
    {
        if (!str_ends_with($className, 'Service'))
        {
            $className .= 'Service';
        }

        $nameSpace = app_path() . '/Services/Updates/' . $className . '.php';

        if (!file_exists(app_path() . '/Services/Updates/' . $className . '.php')) {

            $classContent = "<?php " . PHP_EOL . PHP_EOL .
                "namespace App\\Services\\Updates;" .
                PHP_EOL . PHP_EOL . "class " . $className . PHP_EOL .
                '{' . PHP_EOL.PHP_EOL . '}';

            file_put_contents($nameSpace, $classContent);
        }

        return $nameSpace;
    }

    /**
     * @param String $serviceClass
     * @param String $methodName
     */
    public static function createMethod(String $serviceClass, String $methodName, array $option = null)
    {

        /**
         * Check if the service exists
         */
        if (!file_exists(app_path() . '/Services/Updates/' . $serviceClass . '.php')) {
            throw new UpdateServiceNotFoundException('La classe '.$serviceClass.' n\'existe pas!');
        }

        /**
         * Check if the method exists in the service class
         * Create the method with the option if not null (public, protected, static ...)
         */
        if (!method_exists('App\\Services\\Updates\\' . $serviceClass, $methodName)) {

            $filename = app_path() . '/Services/Updates/' . $serviceClass . '.php';
            $content = file($filename);

            $lines = count($content);
            $content[$lines-1] = 'public function ' . $methodName . '() {' . PHP_EOL . PHP_EOL . '}' . PHP_EOL . PHP_EOL . '}';

            file_put_contents($filename, $content);
        }

    }
}
