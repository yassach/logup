<?php

namespace Rhinos\UpdateLogging\app\Console;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:update-logging';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install all of the update logging resources';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->comment('Publishing updateLogging Configuration...');
        $this->callSilent('vendor:publish', ['--tag' => 'update-logging-config']);

        $this->comment('Publishing updateLogging assets...');
        $this->callSilent('vendor:publish', ['--tag' => 'update-logging-assets']);
        
        $this->comment('Publishing updateLogging migrations...');
        $this->callSilent('vendor:publish', ['--tag' => 'update-logging-migrations']);

        if (!file_exists(app_path() . '/Services')){
            $this->info('<fg=magenta>Creating /Services folder.</>');
            mkdir(app_path() . '/Services');
        }

        if (!file_exists(app_path() . '/Services/Updates'))
        {
            $this->info('<fg=magenta>Creating /Services/Updates folder.</>');
            mkdir(app_path() . '/Services/Updates');
        }

        $this->info('<fg=cyan>updateLogging scaffolding installed successfully.</>');

        $this->line('<fg=green>run: php artisan migrate</>');
    }

}
