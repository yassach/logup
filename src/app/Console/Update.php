<?php

namespace Rhinos\UpdateLogging\app\Console;

use BadMethodCallException;
use Illuminate\Console\Command;
use Rhinos\UpdateLogging\app\Main\UpdateMain;
use Rhinos\UpdateLogging\app\Repositories\UpdateRepository;
use Rhinos\UpdateLogging\app\Exceptions\UpdateNotFoundException;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update {alias} {--method=} {--year=} {--month=} {--week=} {--frequency=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update job with argument alias';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $options = $this->options();

        try {

            /**
             * Get the update
             */
            $update = UpdateRepository::findByAlias($this->argument('alias'));

            /**
             * Run the update(s)
             */
            UpdateMain::run($update, $options);
        }
        catch (UpdateNotFoundException | BadMethodCallException | \Exception $e) {
            error_log($e->getMessage());
            exit();
        }

    }

}
