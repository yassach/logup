<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateAndLogTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('flow_log_details');
        Schema::dropIfExists('flow_log');
        Schema::dropIfExists('updates_step_log');
        Schema::dropIfExists('updates_step');
        Schema::dropIfExists('updates_log');
        Schema::dropIfExists('updates');

        Schema::create('updates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alias')->index()->unique();
            $table->text('description')->nullable();
            $table->enum('frequency', ['quotidienne', 'hebdomadaire', 'mensuelle', 'ponctuelle'])->nullable();
            $table->string('duration', 50)->nullable();
            $table->string('ins_type_id')->nullable()->default('[1]');
            $table->tinyInteger('maintenance')->default('0');
            $table->tinyInteger('has_admin')->default('0');
            $table->tinyInteger('active')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('update_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('update_id');
            $table->year('year')->nullable();
            $table->tinyInteger('month')->nullable();
            $table->tinyInteger('week')->nullable();
            $table->enum('frequency', ['quotidienne', 'hebdomadaire', 'mensuelle', 'ponctuelle'])->nullable();
            $table->dateTime('data_date')->nullable();
            $table->integer('owner_id')->nullable();
            $table->dateTime('start_date')->nullable()->useCurrent();
            $table->dateTime('end_date')->nullable();
            $table->timestamps();
            $table->foreign('update_id')->references('id')->on('updates')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('update_steps', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('update_id')->index();
            $table->string('name');
            $table->string('alias');
            $table->text('description')->nullable();
            $table->string('type', 50);
            $table->tinyInteger('order');
            $table->enum('frequency', ['quotidienne', 'hebdomadaire', 'mensuelle', 'ponctuelle'])->nullable();
            $table->string('duration', 50)->nullable();
            $table->string('ins_type_id')->nullable()->default('[1]');
            $table->tinyInteger('active')->default('1');
            $table->timestamps();
            $table->unique(['update_id', 'type', 'alias']);
            $table->foreign('update_id')->references('id')->on('updates')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('update_step_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('update_log_id')->index();
            $table->unsignedInteger('update_step_id')->index();
            $table->integer('owner_id');
            $table->dateTime('start_date')->useCurrent();
            $table->dateTime('end_date')->nullable();
            $table->enum('status', ['running', 'fail', 'success'])->default('running');
            $table->text('status_details')->nullable();
            $table->timestamps();
            $table->foreign('update_log_id')->references('id')->on('update_logs')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('update_step_id')->references('id')->on('update_steps')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('flow_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('update_step_id')->nullable()->index();
            $table->unsignedInteger('owner_id')->nullable();
            $table->string('source');
            $table->string('destination');
            $table->integer('total_lines_imported');
            $table->integer('total_lines_stored');
            $table->string('status');
            $table->dateTime('start_date')->useCurrent();
            $table->dateTime('end_date')->nullable();
            $table->timestamps();
        });

        Schema::create('flow_log_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('flow_id')->index();
            $table->integer('line_number');
            $table->string('status');
            $table->text('data');
            $table->timestamps();
            $table->foreign('flow_id')->references('id')->on('flow_logs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flow_log_details');
        Schema::dropIfExists('flow_logs');
        Schema::dropIfExists('update_step_logs');
        Schema::dropIfExists('update_steps');
        Schema::dropIfExists('update_logs');
        Schema::dropIfExists('updates');
    }
}
