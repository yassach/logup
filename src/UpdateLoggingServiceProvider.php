<?php

namespace Rhinos\UpdateLogging;

use Illuminate\Support\ServiceProvider;
use Rhinos\UpdateLogging\app\Console\Update;
use Rhinos\UpdateLogging\app\Console\Install;

class UpdateLoggingServiceProvider extends ServiceProvider
{

    /**
     *
     */
    public function boot()
    {
        $this->registerCommands();

        $this->registerPublishing();

        $this->registerViews();

        $this->registerRoutes();

        $this->registerAssets();
    }

    /**
     *
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/update.php', 'update'
        );
    }

    /**
     * Register all the commands
     */
    public function registerCommands(): void
    {
        $this->commands([
            Update::class,
            Install::class,
        ]);
    }

    /**
     * Register all the publishing
     */
    public function registerPublishing(): void
    {
        $this->publishes([
            __DIR__ . '/database/migrations' => $this->app->databasePath() . '/migrations'
        ], 'update-logging-migrations');

        $this->publishes([
            __DIR__ . '/config/update.php' => config_path('update.php'),
        ], 'update-logging-config');
    }

    /**
     * Register all the views
     */
    public function registerViews(): void
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'logup');
    }

    /**
     * Register all the routes
     */
    public function registerRoutes(): void
    {
        include __DIR__ . '/routes/api.php';
    }

    public function registerAssets()
    {
        $this->publishes([
            __DIR__.'/resources/assets' => public_path('logup'),
        ], 'update-logging-assets');
    }
}
