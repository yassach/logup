<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>

<h1> Hello from Package.Index</h1>

@if(!$stepLogs->isEmpty())
    <table>
        @foreach($stepLogs as $stepLog)
            <tr>
                {{$stepLog}}
                <td>{{$stepLog->status}}</td>
                <td>{{$stepLog->status}}</td>
                <td>{{$stepLog->status}}</td>
            </tr>
        @endforeach
    </table>
@endif

<script>
    console.log('index view')
</script>

