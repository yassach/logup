<?php

use Illuminate\Support\Facades\Route;
use Rhinos\UpdateLogging\app\Controllers\UpdateController;


Route::get('/api/vue', function (){
    return view('logup::index');
} );


Route::prefix('/api/update')
->name('update.')
->group(function ()
{
    
    Route::get('/', [UpdateController::class, 'index'])->name('index');
    Route::get('/show/{id}', [UpdateController::class, 'show'])->name('show');
    Route::post('/create', [UpdateController::class, 'store'])->name('store');

    Route::prefix('attach')->name('attach.')->group(function (){
        Route::get('/', [UpdateController::class, 'getUpdates'])->name('updates');
        Route::post('/step', [UpdateController::class, 'attachStepToUpdate'])->name('step');
        Route::get('/steps', [UpdateController::class, 'getSteps'])->name('steps');
    });

});

