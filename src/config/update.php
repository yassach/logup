<?php

use Laravel\Telescope\Http\Middleware\Authorize;

return [

    'service_namespace' => 'Rhinos\\UpdateLogging\\app\\Services\\',

//    'service_namespace' => 'Rhinos\\UpdateLogging\\app\\Services',

    'middleware' => [
        'web',
        Authorize::class,
        'auth'
    ],

    'middleware_rhinos' => 'rhinos',

    /*
    |--------------------------------------------------------------------------
    | UpdateLogging Temporary File Uploads Endpoint Configuration
    |--------------------------------------------------------------------------
    |
    | UpdateLogging handles file uploads by storing uploads in a temporary directory
    | before the file is validated and stored permanently. All file uploads
    | are directed to a global endpoint for temporary storage. The config
    | items below are used for customizing the way the endpoint works.
    |
    */

    'temporary_file_upload' => [
        'disk' => null,        // Example: 'local', 's3'              Default: 'default'
        'rules' => null,       // Example: ['file', 'mimes:png,jpg']  Default: ['required', 'file', 'max:12288'] (12MB)
        'directory' => null,   // Example: 'tmp'                      Default  'livewire-tmp'
        'middleware' => null,  // Example: 'throttle:5,1'             Default: 'throttle:60,1'
        'preview_mimes' => [   // Supported file types for temporary pre-signed file URLs.
            'png', 'gif', 'bmp', 'svg', 'wav', 'mp4',
            'mov', 'avi', 'wmv', 'mp3', 'm4a',
            'jpg', 'jpeg', 'mpga', 'webp', 'wma',
        ],
        'max_upload_time' => 5, // Max duration (in minutes) before an upload gets invalidated.
    ],

    'services' => __DIR__. '/../../../../../../app/MesClasses'

];
