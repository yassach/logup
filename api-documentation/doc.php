<?php

require("./vendor/autoload.php");
$openapi = \OpenApi\Generator::scan(['./src/app/Controllers']);
header('Content-Type: application/json');
echo $openapi->toJson();