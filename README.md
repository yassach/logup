# Update Logging package

Since 2021 by Rhinos Dev Team.

## Download

Paste this command in your terminal
```bash
composer require yassach/logup
```

## Installation

Paste this commands in your terminal
```bash
php artisan install:update-logging
php artisan migrate
```

This will publish config file & migrations to your project

## API Documentation

Check Swagger documentation.